<?php

$txt_file = "A Y
B X
C Z";

$rounds = explode("\n", $txt_file);

$elfInputToAction = ["A" => "rock", "B" => "paper", "C" => "scissor"];
$playerInputToAction = ["X" => "rock", "Y" => "paper", "Z" => "scissor"];
$actionToScores = ["rock" => 1, "paper" => 2, "scissor" => 3];
$winCases = ["rock" => "scissor", "scissor" => "paper", "paper" => "rock"];
$playerInputToNeed = ["X" => "lose", "Y" => "draw", "Z" => "win"];
$totalScore = 0;

foreach($rounds as $round) {
	$inputs = explode(" ", $round);
	$elfInput = trim($inputs[0]);
	$playerInput = trim($inputs[1]);

	// Part 1
	// $totalScore += countPlayerScore($elfInputToAction[$elfInput], $playerInputToAction[$playerInput]);
	
	// Part 2
	$playerAction = getActionDependingOnNeed($elfInputToAction[$elfInput], $playerInput);
	$totalScore += countPlayerScore($elfInputToAction[$elfInput], $playerAction);
}

function countPlayerScore($elfAction, $playerAction) {
	global $winCases, $actionToScores;
	
	if(array_search($elfAction, $winCases) == $playerAction) {
		$winScore = 6;
	} else if (array_search($playerAction, $winCases) == $elfAction) {
		$winScore = 0;
	} else {
		$winScore = 3;
	}

	return $actionToScores[$playerAction] + $winScore;
}

function getActionDependingOnNeed($elfAction, $playerInput) {
	global $winCases, $playerInputToNeed;
	
	if($playerInputToNeed[$playerInput] == "draw") {
		return $elfAction;
	} else if($playerInputToNeed[$playerInput] == "lose") {
		return $winCases[$elfAction];
	} else if ($playerInputToNeed[$playerInput] == "win") {
		return array_search($elfAction, $winCases);
	}
}

var_dump($totalScore);