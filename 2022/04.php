<?php

$txt_file = "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";

$lines = explode("\n", $txt_file);
$countIsInRange = 0;

foreach($lines as $line) {
	$pairs = explode(",", trim($line));
	
	$elf1 = explode("-", $pairs[0]);
	$elf1 = range($elf1[0], $elf1[1]);
	
	$elf2 = explode("-", $pairs[1]);
	$elf2 = range($elf2[0], $elf2[1]);
	
    // $elf1ContainsElf2 = isFullyInRange($elf1, $elf2);
	// $elf2ContainsElf1 = isFullyInRange($elf2, $elf1);

	$elf1ContainsElf2 = isInRange($elf1, $elf2);
	$elf2ContainsElf1 = isInRange($elf2, $elf1);
	
	if($elf1ContainsElf2 || $elf2ContainsElf1) {
		$countIsInRange += 1;	
	}
}

// Part 1
function isFullyInRange($lookFor, $searchIn) {
	$isContain = true;
	foreach($lookFor as $num) {
		if(in_array($num, $searchIn) == false) {
			$isContain = false;
		}
	}
	return $isContain;
}

// Part 2
function isInRange($lookFor, $searchIn) {
	$isContain = false;
	foreach($lookFor as $num) {
		if(in_array($num, $searchIn)) {
			$isContain = true;
		}
	}
	return $isContain;
}

var_dump($countIsInRange);