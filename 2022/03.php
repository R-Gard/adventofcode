<?php

$txt_file = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";

$lines = explode("\n", $txt_file);

$items = [];
$lowerAlphabetToValue = array_combine(range("a", "z"), range(1, 26));
$upperAlphabetToValue = array_combine(range("A", "Z"), range(27, 52));
$alphabetToValue = array_merge($lowerAlphabetToValue, $upperAlphabetToValue);
$total = 0;

// Part 1
// foreach($lines as $line) {
// 	$half = strlen(trim($line))/2;
// 	$bag1 = substr($line, 0, $half);
// 	$bag2 = substr($line, $half);
	
// 	for($i=0; $i<$half; $i++) {
// 		if (str_contains($bag2, $bag1[$i])) {
// 			$items[] = $bag1[$i];
// 			break;
// 		}
// 	}
// }

// Part 2
for($i=0; $i<count($lines)-2; $i+=3) {
	$bag1 = trim($lines[$i]);
	$bag2 = trim($lines[$i+1]);
	$bag3 = trim($lines[$i+2]);
	
	for($y=0; $y<strlen($bag1); $y++) {
		if (str_contains($bag2, $bag1[$y])) {
			if (str_contains($bag3, $bag1[$y])) {
				$items[] = $bag1[$y];
				break;
			}
		}
	}
}

foreach($items as $item) {
	$total += $alphabetToValue[$item];
}

var_dump($total);